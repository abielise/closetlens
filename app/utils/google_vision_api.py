import io
import os
from google.cloud import vision
from google.cloud.vision import types

from flask import jsonify
from app import app


class GoogleVisionApi:

    categories = {
        'type': set(),
        'color': set()
    }

    def read_data(self):
        ''' Opens and reads into memory pre-computed terms '''
        colorsFile = open(app.config['COLOR_PATH'], 'r')
        typesFile = open(app.config['TYPE_PATH'], 'r')
        self.categories['color'] = set([line.rstrip('\n') for line in colorsFile])
        self.categories['type'] = set([line.rstrip('\n') for line in typesFile])

    def get_labels(self, filename):
        ''' Returns a list of label annotations for the arg image '''
        color_labels = []
        type_labels = []
        client = vision.ImageAnnotatorClient()
        file_name = os.path.join(
            app.config['UPLOAD_FOLDER'], 
            filename)

        with io.open(file_name, 'rb') as image_file:
            content = image_file.read()

        image = types.Image(content=content)

        response = client.label_detection(image=image)
        labels = response.label_annotations

        print('Labels:')
        for label in labels:
            print(label.description)
        return labels

    def filter_labels(self, labels):
        ''' 
        takes a list of labels and returns a dict of 
        filtered labels for each category 
        '''
        result = {}
        label_set = set([label.description for label in labels])
        self.read_data()
        
        for category_key in self.categories:
            intersect_list = self.categories[category_key].intersection(label_set)
            result[category_key] = [{'description': item.description, 'score': item.score} for item in labels if item.description in intersect_list]

        return result

    def labels_to_tags(self, article, labels):
        result = []
        for label_cat in labels:
            for label in labels[label_cat]:
                label['category'] = label_cat
                label['article_id'] = article['id']
                result.append(label)
        return result

    def get_tags(self, article):
        labels = []
        labels = self.get_labels(article['filename'])
        filtered_labels = self.filter_labels(labels)
        return self.labels_to_tags(article, filtered_labels)

