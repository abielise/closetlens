from flask import render_template, jsonify, abort, make_response, request, redirect, url_for, send_from_directory
from datetime import datetime
from app.models.article import ArticleService, Article
from app.models.tag import TagService
from app.utils import image_utils, google_vision_api, wiki_api
from app import app


@app.route('/')
@app.route('/index')
def index():
    return render_template('index.html', title='Home', user={'username':'friend'})


@app.route('/api/article', methods=['GET'])
def get_clothing():
    ''' list all currently saved articles of clothing '''
    articles = ArticleService().find_all()
    return jsonify({'clothing': articles, 'count': len(articles)})


@app.route('/api/article/<int:article_id>', methods=['GET'])
def get_clothing_by_id(article_id):
    if request.method == 'GET':
        article = ArticleService().find_article_by_id(article_id)
        if article == None:
            abort(404)
        return jsonify({'clothing': article})


@app.route('/api/article/upload', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)

        filename = image_utils.save_file(file)
        
        if filename == False:
            flash('Error while saving file')
            return redirect(request.url)

        saved_article = ArticleService().save_article(filename).serialize()
        tags = google_vision_api.GoogleVisionApi().get_tags(saved_article)
        TagService().save_tags(tags)
        article = ArticleService().find_article_by_id(saved_article['id'])
        return jsonify({'clothing': article}), 201
    # GET
    return render_template('upload.html')


@app.route('/api/refresh-data', methods=['GET'])
def refresh_data():
    wiki_api.WikiApi().refresh_data()
    return jsonify({'data-refresh':'success'})


''' Error handling '''
@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'Not found'}), 404)
