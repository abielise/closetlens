from datetime import datetime
from app.models.tag import TagService

clothing_arr = [
    {
        'id': 1,
        'filename': u'blouse.png',
        'created': str(datetime.now()),
        'updated': str(datetime.now())
    }
]


class Article:
    def __init__(self, _id, filename, created=str(datetime.now()), updated=str(datetime.now())):
        self._id = _id
        self.filename = filename
        self.created = created
        self.updated = updated


    def serialize(self):
        return {
            'id': self._id,
            'filename': self.filename,
            'created': self.created,
            'updated': self.updated
        }


class ArticleService:
    def __init__(self):
        self.tag_service = TagService()


    def find_article_by_id(self, article_id):
        ''' Returns the article if found, otherwise None '''
        for article in clothing_arr:
            if article['id'] == article_id:
                article['tags'] = self.tag_service.find_tags_by_article_id(article_id)
                return article
        return None


    def find_all(self):
        ''' Returns a list of all articles of clothing in memory with tags '''
        result = clothing_arr
        for article in result:
            article['tags'] = self.tag_service.find_tags_by_article_id(article['id'])
        return result


    def save_article(self, filename):
        saved_article = Article(len(clothing_arr) + 1, filename)
        clothing_arr.append(saved_article.serialize())
        return saved_article

