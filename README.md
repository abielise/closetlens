author: Abby Grobbel, 2018

set up virtualenv and run:

`source venv/bin/activate`

Then install requirements:

`pip install -r requirements.txt`

To start the app:

`flask run --port=8080`

Environment variables may be configured in .flaskenv