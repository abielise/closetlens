from urllib import request, parse
from bs4 import BeautifulSoup
from collections import deque
from app import app
import json

class WikiApi:
    BASE_URL = 'https://en.wikipedia.org/w/api.php?'
    TOP_LEVEL_CATEGORY = 'Category:Clothing_by_type'

    def get_color_range(self, color_range):
        result_colors = []
        query_params = {
            'action':'parse',
            'prop':'text',
            'format':'json',
            'page':color_range
        }
        url = self.BASE_URL + parse.urlencode(query_params)
        
        try:
            response = request.urlopen(url)
        except:
            print('Error while requesting data')
            return result_colors
        
        contents = json.loads(response.read().decode('utf8'))
        soup = BeautifulSoup(contents['parse']['text']['*'], 'html.parser')
        table = soup.find('table', class_="wikitable")
        if table:
            for row in table.find_all('tr')[1:]:
                headers = row.find_all('th')
                if headers:
                    text = row.find_all('th')[0].text
                    if text != '':
                        result_colors.append(text)
        return result_colors

    def get_colors(self):
        color_list = []
        color_list += self.get_color_range('List_of_colors:_A–F')
        color_list += self.get_color_range('List_of_colors:_G–M')
        color_list += self.get_color_range('List_of_colors:_N–Z')

        colorsFile = open(app.config['COLOR_PATH'], 'w')

        for color in color_list:
            colorsFile.write(color.lower())

    def request_category_page(self, cmtitle, includeNamespace=True):
        query_params = {
            'action':'query',
            'list':'categorymembers',
            'cmlimit':100,
            'cmprop':'id|title|type',
            'format':'json',
            'cmtitle':cmtitle
        }
        
        if includeNamespace:
            query_params['cmnamespace'] = 14

        url = self.BASE_URL + parse.urlencode(query_params)
        response = request.urlopen(url)
        content = json.loads(response.read().decode('utf8'))
        if content and len(content['query']['categorymembers']):
            return (content.get('title','') for content in content['query']['categorymembers'])
        else:
            return False

    def clean_title(self, title):
        return title[title.find(':')+1:]

    def get_category_levels(self, cmtitle, levels=1):
        ''' rather than searching until a terminal node, just grabs 2 levels deep '''
        result = []
        current = []
        visited = {}

        content = self.request_category_page(cmtitle)
        if content:
            current.extend(content)

        while levels > 0:
            tmp = []
            for category in current:
                if category not in visited:
                    visited[category] = True
                    content = self.request_category_page(category)
                    if content == False:
                        result.append(self.clean_title(category))
                    elif content:
                        tmp.extend(content)
            current = tmp
            levels = levels - 1

        # bring over extra data
        for curr in current:
            if curr not in visited:
                visited[curr] = True
                result.append(self.clean_title(curr))

        result.sort()
        return result


    def get_category_helper(self, cmtitle):
        ''' Searches the category response using bfs '''
        result = []
        d = deque()
        visited = {}
        
        content = self.request_category_page(cmtitle)
        if content:
            d.extend(content)
        
        while (len(d)):
            title = d.popleft()
            if title not in visited:
                visited[title] = True
                child_content = self.request_category_page(title)
                if child_content:
                    d.extend(child_content)
                else:
                    result += self.request_category_page(title, includeNamespace=False)
            else:
                print('seen ' + title + ' before')
        return result

    def get_types(self):
        clothing_list = []
        clothing_list = self.get_category_levels(self.TOP_LEVEL_CATEGORY)
        clothingFile = open(app.config['TYPE_PATH'], "w")

        for article in clothing_list:
            clothingFile.write(article.lower()+'\n')
    
    def refresh_data(self):
        # self.get_colors()
        self.get_types()
