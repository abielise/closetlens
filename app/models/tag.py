tags_arr = [
    {
        'id': 1,
        'article_id': 1,
        'category': u'color',
        'description': u'yellow',
        'score': 0.54
    },
    {
        'id': 2,
        'article_id': 1,
        'category': u'type',
        'description': u'shirt',
        'score': 0.94
    },
]


class Tag:
    def __init__(self, _id, article_id, category, description, score):
        self._id = _id
        self.article_id = article_id
        self.category = category
        self.description = description
        self.score = score

    def serialize(self):
        return {
            'id': self._id,
            'article_id': self.article_id,
            'category': self.category,
            'description': self.description,
            'score': self.score
        }


class TagService:
    def __init__(self, tags = {}):
        self.tags = tags

    def find_tags_by_article_id(self, article_id):
        ''' Returns a list of all tags for an article '''
        tags = []
        for tag in tags_arr:
            if tag['article_id'] == article_id:
                tags.append(tag)
        return tags


    def save_tag(self, _id, article_id, category, description, score):
        saved_tag = Tag(_id, article_id, category, description, score)
        tags_arr.append(saved_tag.serialize())
        return saved_tag


    def save_tags(self, tag_list):
        for tag in tag_list:
            self.save_tag(len(tags_arr)+1, tag['article_id'], tag['category'], tag['description'], tag['score'])

