import os

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'dev'
    UPLOAD_FOLDER = os.path.join('app', 'resources', 'uploads')
    TYPE_PATH = os.path.join('app', 'resources', 'clothing.txt')
    COLOR_PATH = os.path.join('app', 'resources', 'colors.txt')

class DevelopmentConfig(Config):
    DEBUG = True
    ENV = 'development'

class TestingConfig(Config):
    TESTING = True